# oaspy

[![Python: 3.10](https://img.shields.io/badge/python-3.10-blue?logo=python)](https://docs.python.org/3.10/)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![PyPI version](https://badge.fury.io/py/oaspy.svg)](https://pypi.org/project/oaspy/)
---

**oaspy** is a quick-and-dirty tool to generate an [OpenApi 3.x](https://www.openapis.org) specification from an [insomnia V4](https://insomnia.rest/products/insomnia) collections.


## Prerequisites

 - [Python 3.10.x](https://docs.python.org/3.10/)


## Getting Started

We welcome contributions in the form of Pull Requests.

### clone this repo:

```bash
$ git clone git@gitlab.com:HomeInside/oaspy.git
```

### create the environment

```bash
$ python3.10 -m venv .venv
```

```bash
$ source .venv/bin/activate
```

### installing dependencies (and dev dependencies)

```bash
$ (env) pip install --upgrade pip
```

```bash
$ (env) pip install -e .[dev]
```


## How to test

```sh
$ (env) python -m src.oaspy.main --help
```

> the output should be something like
```
Usage: python -m src.oaspy.main [OPTIONS] COMMAND [ARGS]...

  oaspy is a quick-and-dirty tool to generate an OpenApi 3.x specification
  from an insomnia V4 collections.

Options:
  -v, --version  show current version and exit.
  --help         Show this message and exit.

Commands:
  check  Validates the structure of an OpenApi file.
  gen    Generate an OpenApi 3.x file from an Insomnia collection v4.
  info   Shows information from an Insomnia v4 file
```


## Organize and format the source code

```sh
$ (env) isort --profile black --atomic src/
```

```sh
$ (env) black src/
```


## Error checking (ruff lint)

```bash
$ (env) ruff src/
```

automatically correct (if possible) problems related to the lint
```bash
$ (env) ruff --fix src/
```


## TODO

- [x] export to openapi 3.0.x (partial)
- [ ] export to openapi 3.1.x
- [x] add custom info to openapi export (partial)
- [x] handle Insomnia collection v4
- [x] checks that the generated file is according to the openapi schema
- [x] provide information from the Insomnia file as a summary
- [x] group folders
- [ ] GET, POST, PUT, DELETE, PATCH (partial)
- [x] JSON (`Content-Type: application/json`) requests
- [x] multipart/form-data requests
- [x] Environments vars
- [x] description from requests
- [ ] Environments with parameters
- [x] Bearer authorization
- [ ] OAuth (Basic)


## Inspiration

**oaspy** takes ideas from many open source projects:

- https://spec.openapis.org/oas/latest.html
- https://github.com/OAI/OpenAPI-Specification/blob/main/schemas/v3.1/schema.json
- https://www.jsonschemavalidator.net
- https://json-schema.hyperjump.io
- https://python-jsonschema.readthedocs.io/en/stable/
- https://marketplace.visualstudio.com/items?itemName=42Crunch.vscode-openapi
- https://editor.swagger.io
- https://editor-next.swagger.io
- https://insomnia.rest/products/insomnia
- https://swagger.io/docs/specification/describing-request-body/multipart-requests/
- https://swagger.io/docs/specification/describing-request-body/file-upload/


## License

This project is licensed under the terms of the [MIT.](https://opensource.org/license/mit/) license.

The full text of this license can be found in the [LICENSE.](./LICENSE) file.


## How to Contribute

For any questions, comments, suggestions or contributions, go to the [issues.](https://gitlab.com/HomeInside/oaspy/-/issues) section.
Before opening a new issue, check the existing ones to find a solution (possibly already existing) to the problem you are facing.
